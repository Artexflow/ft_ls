/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 02:18:29 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 08:42:09 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

void			ft_putstr(const char *str);
void			*ft_memcpy(void *dest, const void *src, size_t n);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_print(const char *str, int fd, int ret);
char			*ft_strdup(const char *str);

#endif
