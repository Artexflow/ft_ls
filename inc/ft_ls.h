/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 01:48:49 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/21 03:52:09 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H
# define STRUCT_DIRENT struct dirent
# include <dirent.h>
# include <sys/stat.h>
# include "direntlst.h"

typedef struct			s_options_flags
{
	int					l;
	int					r;
	int					rmaj;
	int					a;
	int					t;
	int					no_option;
	int					multiple;
}						t_opflag;

typedef struct			s_pathlst
{
	char				*path;
	struct s_pathlst	*next;
}						t_pathlst;

typedef struct			s_env
{
	t_opflag			opflag;
	t_pathlst			*pathlst;
}						t_env;

int						check_input(int ac, char **av, t_env *env);
void					add_path(char *path, t_pathlst **lst);
void					main_loop(t_pathlst *lst, t_opflag *opflag);
int						print_error(char *incriminated_var, int ret);
char					*path_builder(char *path, char *file_name);
void					set_infos(t_direntlst *lst, t_opflag *opflag);
t_direntlst				*getdirent(char *path, t_opflag *opflag);
void					sort(t_direntlst **lst, t_opflag *opflag);
void					time_sort(t_direntlst **lst);
void					reverse_time_sort(t_direntlst **lst);
void					recursive_call(t_direntlst *lst, t_opflag *opflag);

#endif
