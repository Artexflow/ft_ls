/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   direntlst.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/21 03:51:28 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/21 03:56:16 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIRENTLST_H
# define DIRENTLST_H
# include <sys/stat.h>
# include <dirent.h>

typedef struct dirent		t_dirent;
typedef struct stat			t_stat;
typedef struct s_direntlst	t_direntlst;

struct						s_direntlst
{
	char					*path;
	char					*original_path;
	t_dirent				*dirent;
	t_stat					*stat;
	t_direntlst				*next;
};

#endif
