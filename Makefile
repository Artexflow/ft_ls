# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nsierra- <nsierra-@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/02 08:22:33 by nsierra-          #+#    #+#              #
#    Updated: 2014/04/15 08:43:24 by nsierra-         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	ft_ls 
INC_FILES	=	ft_ls.h \
				libft.h
SRC_FILES	=	main.c \
				check_input.c \
				loop.c \
				path_builder.c \
				print_error.c \
				add_path.c \
				getdirent.c \
				sort.c \
				time_sort.c \
				set_infos.c \
				recursive_call.c \
				ft_putstr.c \
				ft_memcpy.c \
				ft_print.c \
				ft_strcmp.c \
				ft_strdup.c
SRC			=	$(addprefix src/, $(SRC_FILES))
INC			=	$(addprefix inc/, $(INC_FILES))
OBJ			=	$(SRC:.c=.o)

CC			=	gcc

#CFLAGS		=	-g3 -pedantic-errors -Wextra -Wall -Waggregate-return -Werror \
#				-Waggressive-loop-optimizations -Wno-attributes -Wcast-align \
#				-Wconversion -Wcoverage-mismatch -Wdate-time \
#				-Wno-cpp -Wdisabled-optimization -Wdouble-promotion  \
#				-Wfloat-equal -Wformat-nonliteral -Wformat-security \
#				-Wformat-y2k -Wjump-misses-init -Wimplicit -Winit-self \
#				-Winline -Winvalid-pch -Wunsafe-loop-optimizations \
#				-Wlogical-op -Wmissing-include-dirs \
#				-Woverlength-strings -Wpacked  -Wpacked-bitfield-compat \
#				-Wpointer-arith -Wredundant-decls -Wshadow \
#				-Wsign-conversion -Wfloat-conversion \
#				-Wsizeof-pointer-memaccess -Wstack-protector -Wstrict-overflow \
#				-Wmissing-format-attribute -Wswitch-default -Wswitch-enum \#-Wsync-nand -Wtrampolines -Wundef \
#				-Wunsuffixed-float-constants -Wunused  -Wunused-local-typedefs \
#				-Wunused-but-set-variable -Wvariadic-macros \
#				-Wvector-operation-performance -Wvla -Wwrite-strings

CFLAGS		=	-g3 -Wall -Wextra -Werror

LDFLAGS		=	-I./inc/

all:			$(NAME)

$(NAME):		$(INC) $(OBJ)
					$(CC) $(CFLAGS) $(OBJ) -o $@

%.o:			%.c
					$(CC) $(CFLAGS) $(LDFLAGS) -c $< -o $@

clean:
					rm -rf $(OBJ)

fclean:			clean
					rm -f $(NAME)

re:				fclean all
