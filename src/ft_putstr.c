/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 02:17:34 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 05:36:11 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

static size_t	ft_strlen(const char *str)
{
	const char	*keep;

	keep = str;
	while (*str != '\0')
		++str;
	return ((size_t)(str - keep));
}

void			ft_putstr(const char *str)
{
	write(1, str, ft_strlen(str));
}
