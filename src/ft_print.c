/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 03:01:53 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 03:08:20 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stddef.h>

size_t				ft_strlen(const char *str)
{
	const char		*cursor;

	cursor = str;
	while (*cursor != '\0')
		++cursor;
	return ((size_t)(cursor - str));
}

int					ft_print(const char *str, int fd, int ret)
{
	if (str)
		write(fd, str, ft_strlen(str));
	return (ret);
}
