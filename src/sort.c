/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 15:25:31 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 08:14:40 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <stddef.h>
#include <dirent.h>
#include <libft.h>
#include <ft_ls.h>

static void			swap(t_direntlst *de1, t_direntlst *de2)
{
	char			*tmp_char;
	struct dirent	*tmp_dirent;
	struct stat		*tmp_stat;

	tmp_char = de1->original_path;
	de1->original_path = de2->original_path;
	de2->original_path = tmp_char;
	tmp_dirent = de1->dirent;
	de1->dirent = de2->dirent;
	de2->dirent = tmp_dirent;
	if (de1->stat && de2->stat)
	{
		tmp_stat = de1->stat;
		de1->stat = de2->stat;
		de2->stat = tmp_stat;
	}
}

static void			reverse_sort(t_direntlst **lst)
{
	t_direntlst		*cursor;
	t_direntlst		*prev;
	int				flag_sorted;

	flag_sorted = 0;
	while (flag_sorted == 0)
	{
		flag_sorted = 1;
		cursor = *lst;
		prev = *lst;
		cursor = cursor->next;
		while (cursor != NULL)
		{
			if (ft_strcmp(cursor->dirent->d_name, prev->dirent->d_name) > 0)
			{
				flag_sorted = 0;
				swap(cursor, prev);
			}
			cursor = cursor->next;
			prev = prev->next;
		}
	}
	return ;
}

static void			regular_sort(t_direntlst **lst)
{
	t_direntlst		*cursor;
	t_direntlst		*prev;
	int				flag_sorted;

	flag_sorted = 0;
	while (flag_sorted == 0)
	{
		flag_sorted = 1;
		cursor = *lst;
		prev = *lst;
		if (cursor)
			cursor = cursor->next;
		while (cursor != NULL)
		{
			if (ft_strcmp(cursor->dirent->d_name, prev->dirent->d_name) < 0)
			{
				flag_sorted = 0;
				swap(cursor, prev);
			}
			cursor = cursor->next;
			prev = prev->next;
		}
	}
}

void				sort(t_direntlst **lst, t_opflag *opflag)
{
	if (opflag->t == 1 && opflag->r == 1)
		reverse_time_sort(lst);
	else if (opflag->t == 1)
		time_sort(lst);
	else if (opflag->r == 1)
		reverse_sort(lst);
	else
		regular_sort(lst);
}
