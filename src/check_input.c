/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_options.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 02:26:50 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 05:35:57 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <libft.h>
#include <ft_ls.h>

static int		illegal_option(char c)
{
	ft_putstr("ft_ls: illegal option -- ");
	write(1, &c, 1);
	ft_putstr("\nusage: fl_ls [-arRlt] [file ...]\n");
	return (0);
}

static int		get_options(char *option, t_env *env)
{
	while (*option != '\0')
	{
		if (*option == 'l')
			env->opflag.l = 1;
		else if (*option == 'r')
			env->opflag.r = 1;
		else if (*option == 'R')
			env->opflag.rmaj = 1;
		else if (*option == 'a')
			env->opflag.a = 1;
		else if (*option == 't')
			env->opflag.t = 1;
		else if (*option == '-')
			env->opflag.no_option = 1;
		else
			return (illegal_option(*option));
		option++;
	}
	return (1);
}

int				check_input(int ac, char **av, t_env *env)
{
	int			i;
	int			status;

	i = 1;
	status = 1;
	while (ac > 1)
	{
		if (av[i][0] == '-'
				&& env->opflag.no_option == 0
				&& status == 1
				&& av[i][1] != '\0')
			status = get_options(av[i++] + 1, env);
		else
			break ;
		--ac;
	}
	while (status && ac > 1)
	{
		if (ac > 2)
			env->opflag.multiple = 1;
		status = 2;
		add_path(av[i++], &env->pathlst);
		ac--;
	}
	return (status);
}
