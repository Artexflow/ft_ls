/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 01:31:15 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 08:49:03 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <unistd.h>
#include <ft_ls.h>

void	set_env(t_env *e)
{
	e->opflag.l = 0;
	e->opflag.r = 0;
	e->opflag.rmaj = 0;
	e->opflag.a = 0;
	e->opflag.t = 0;
	e->opflag.no_option = 0;
	e->opflag.multiple = 0;
	e->pathlst = NULL;
}

int		main(int ac, char **av)
{
	t_env	env;
	int		status;

	status = 0;
	set_env(&env);
	if (ac > 1 && !(status = check_input(ac, av, &env)))
		return (0);
	else if (status != 2)
		add_path((char *)".", &env.pathlst);
	main_loop(env.pathlst, &env.opflag);
	return (0);
}
