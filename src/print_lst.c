/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_lst.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 03:22:18 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 05:34:49 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <ft_ls.h>

void			main_loop(t_pathlst *lst, t_opflag *opflag)
{
	t_pathlst	*cursor;
	t_direntlst	*direntlst;

	cursor = lst;
	while (cursor != NULL)
	{
		direntlst = getdirent(lst->path, opflag);
		cursor = cursor->next;
	}
}
