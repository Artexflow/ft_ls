/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_path.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/14 22:05:32 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 08:48:26 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <libft.h>
#include <ft_ls.h>

static int		is_valid_path(char *path)
{
	struct stat	stat_info;

	if (lstat(path, &stat_info) == -1)
	{
		ft_putstr("ft_ls: ");
		ft_putstr(path);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		ft_putstr("\n");
		return (0);
	}
	return (1);
}

void			add_path(char *path, t_pathlst **lst)
{
	t_pathlst	*new_elem;

	if (!is_valid_path(path))
		return ;
	new_elem = NULL;
	if (*lst == NULL && (new_elem = malloc(sizeof(t_pathlst))))
	{
		new_elem->path = ft_strdup(path);
		new_elem->next = NULL;
		*lst = new_elem;
	}
	else if ((new_elem = malloc(sizeof(t_pathlst))))
	{
		new_elem->path = ft_strdup(path);
		new_elem->next = *lst;
		*lst = new_elem;
	}
	else
		ft_putstr("Error while building path list.\n");
}
