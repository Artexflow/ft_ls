/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 08:06:46 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 11:09:11 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <libft.h>
#include <ft_ls.h>

static void			swap(t_direntlst *de1, t_direntlst *de2)
{
	char			*tmp_char;
	struct dirent	*tmp_dirent;
	struct stat		*tmp_stat;

	tmp_char = de1->original_path;
	de1->original_path = de2->original_path;
	de2->original_path = tmp_char;
	tmp_dirent = de1->dirent;
	de1->dirent = de2->dirent;
	de2->dirent = tmp_dirent;
	if (de1->stat && de2->stat)
	{
		tmp_stat = de1->stat;
		de1->stat = de2->stat;
		de2->stat = tmp_stat;
	}
}

static void			reg_check(t_direntlst *cursor, t_direntlst *prev, int *flag)
{
	if (ft_strcmp(cursor->dirent->d_name, prev->dirent->d_name) < 0)
	{
		*flag = 0;
		swap(cursor, prev);
	}
}

static void			rev_check(t_direntlst *cursor, t_direntlst *prev, int *flag)
{
	if (ft_strcmp(cursor->dirent->d_name, prev->dirent->d_name) > 0)
	{
		*flag = 0;
		swap(cursor, prev);
	}
}

void				reverse_time_sort(t_direntlst **lst)
{
	t_direntlst		*cursor;
	t_direntlst		*prev;
	int				flag_sorted;

	flag_sorted = 0;
	while (flag_sorted == 0)
	{
		flag_sorted = 1;
		cursor = *lst;
		prev = *lst;
		if (cursor)
			cursor = cursor->next;
		while (cursor != NULL && cursor->stat && prev->stat)
		{
			if (cursor->stat->st_mtime == prev->stat->st_mtime)
				rev_check(cursor, prev, &flag_sorted);
			if (cursor->stat->st_mtime < prev->stat->st_mtime)
			{
				flag_sorted = 0;
				swap(cursor, prev);
			}
			cursor = cursor->next;
			prev = prev->next;
		}
	}
}

void				time_sort(t_direntlst **lst)
{
	t_direntlst		*cursor;
	t_direntlst		*prev;
	int				flag_sorted;

	flag_sorted = 0;
	while (flag_sorted == 0)
	{
		flag_sorted = 1;
		cursor = *lst;
		prev = *lst;
		if (cursor)
			cursor = cursor->next;
		while (cursor != NULL && cursor->stat && prev->stat)
		{
			if (cursor->stat->st_mtime == prev->stat->st_mtime)
				reg_check(cursor, prev, &flag_sorted);
			if (cursor->stat->st_mtime > prev->stat->st_mtime)
			{
				flag_sorted = 0;
				swap(cursor, prev);
			}
			cursor = cursor->next;
			prev = prev->next;
		}
	}
}
