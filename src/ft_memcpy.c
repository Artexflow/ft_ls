/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 09:36:38 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/12 09:37:00 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char		*keep;
	const char	*src_as_cc;

	keep = dest;
	src_as_cc = src;
	while (n-- != 0)
		*keep++ = *src_as_cc++;
	return (keep);
}
