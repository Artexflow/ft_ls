/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 08:44:48 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 11:08:37 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <dirent.h>
#include <stddef.h>
#include <libft.h>
#include <ft_ls.h>

static void			print_path(char *presumed_path)
{
	size_t			i;
	size_t			j;
	char			c;

	i = 0;
	j = 0;
	c = -1;
	while (presumed_path[i] != '\0')
	{
		if (presumed_path[i] == '/')
			j = i;
		++i;
	}
	if (i != 0 && presumed_path[i - 1] != '/' && i - 1 != j
		&& presumed_path[j] == '/')
	{
		c = presumed_path[j + 1];
		presumed_path[j + 1] = '\0';
	}
	ft_putstr(presumed_path);
	ft_putstr(":\n");
	if (c != -1)
		presumed_path[j + 1] = c;
}

static void			printlst(t_direntlst *lst, t_opflag *opflag)
{
	t_direntlst		*cursor;

	cursor = lst;
	if (cursor && !cursor->path && (opflag->rmaj || opflag->multiple))
		print_path(cursor->original_path);
	else if (cursor && (opflag->rmaj || opflag->multiple))
		print_path(cursor->path);
	while (cursor != NULL)
	{
		if (opflag->rmaj || opflag->multiple)
			ft_putstr("\t");
		ft_putstr(cursor->dirent->d_name);
		ft_putstr("\n");
		cursor = cursor->next;
	}
}

static void			destroy_direntlst(t_direntlst *lst)
{
	t_direntlst		*cursor;
	t_direntlst		*tmp;

	cursor = lst;
	while (cursor != NULL)
	{
		if (cursor->path)
			free(cursor->path);
		if (cursor->dirent)
			free(cursor->dirent);
		if (cursor->stat)
			free(cursor->stat);
		tmp = cursor;
		cursor = cursor->next;
		free(tmp);
	}
}

static void			destroy_pathlst(t_pathlst *lst)
{
	t_pathlst		*cursor;
	t_pathlst		*tmp;

	cursor = lst;
	while (cursor != NULL)
	{
		if (cursor->path)
			free(cursor->path);
		tmp = cursor;
		cursor = cursor->next;
		free(tmp);
	}
}

void				main_loop(t_pathlst *lst, t_opflag *opflag)
{
	t_pathlst		*cursor;
	t_pathlst		*keep;
	t_direntlst		*direntlst;

	cursor = lst;
	keep = lst;
	while (cursor != NULL)
	{
		direntlst = getdirent(cursor->path, opflag);
		if (opflag->t == 1 || opflag->l == 1 || opflag->rmaj == 1)
			set_infos(direntlst, opflag);
		sort(&direntlst, opflag);
		printlst(direntlst, opflag);
		if (opflag->rmaj == 1)
			recursive_call(direntlst, opflag);
		destroy_direntlst(direntlst);
		cursor = cursor->next;
		if (cursor && (opflag->rmaj || opflag->multiple))
			ft_putstr("\n");
	}
	destroy_pathlst(keep);
}
