/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 08:38:07 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 08:43:41 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>

static size_t	ft_strlen(const char *str)
{
	const char	*keep;

	keep = str;
	while (*keep != '\0')
		++keep;
	return ((size_t)(keep - str));
}

char			*ft_strdup(const char *str)
{
	char		*new_str;
	char		*keep;

	new_str = NULL;
	if (!(new_str = malloc(sizeof(char) * ft_strlen(str) + 1)))
		return (NULL);
	keep = new_str;
	while (*str != '\0')
	{
		*new_str = *str;
		++new_str;
		++str;
	}
	*new_str = '\0';
	return (keep);
}
