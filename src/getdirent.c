/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getdirent.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/14 23:39:18 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 06:42:54 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <libft.h>
#include <ft_ls.h>

static int			self_or_parent(char *name)
{
	if (name)
	{
		if ((name[0] == '.' && name[1] == '\0')
			|| (name[0] == '.' && name[1] == '.' && name[2] == '\0'))
			return (1);
	}
	return (0);
}

static void			add_ent(char *path, struct dirent *dirent, t_direntlst **l)
{
	t_direntlst		*new_elem;
	struct dirent	*dirent_cp;

	new_elem = NULL;
	dirent_cp = NULL;
	if (!(new_elem = malloc(sizeof(t_direntlst))))
		return ;
	new_elem->path = NULL;
	new_elem->stat = NULL;
	new_elem->original_path = path;
	if (!(dirent_cp = malloc(sizeof(struct dirent))))
	{
		free(new_elem);
		return ;
	}
	ft_memcpy(dirent_cp, dirent, sizeof(struct dirent));
	new_elem->dirent = dirent_cp;
	if (*l == NULL)
		new_elem->next = NULL;
	else
		new_elem->next = *l;
	*l = new_elem;
}

t_direntlst			*getdirent(char *path, t_opflag *opflag)
{
	DIR				*dirp;
	STRUCT_DIRENT	*dirent;
	t_direntlst		*ret_lst;

	ret_lst = NULL;
	if (!(dirp = opendir(path)))
	{
		print_error(path, 0);
		return (NULL);
	}
	while ((dirent = readdir(dirp)) != NULL)
	{
		if (opflag->a == 1 && dirent->d_name[0] == '.'
			&& !self_or_parent(dirent->d_name))
			add_ent(path, dirent, &ret_lst);
		else if (dirent->d_name[0] != '.')
			add_ent(path, dirent, &ret_lst);
	}
	closedir(dirp);
	return (ret_lst);
}
