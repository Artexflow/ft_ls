/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_call.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/20 22:09:07 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/20 22:09:08 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ft_ls.h>
#include <libft.h>

void				recursive_call(t_direntlst *lst, t_opflag *opflag)
{
	t_direntlst		*cursor;
	t_pathlst		*pathlst;
	DIR				*dirp;
	struct stat		stat_i;

	cursor = lst;
	pathlst = NULL;
	while (cursor != NULL)
	{
		if (lstat(cursor->path, &stat_i) == 0)
		{
			if (stat_i.st_mode & S_IFDIR
				&& (dirp = opendir(cursor->path)))
			{
				closedir(dirp);
				add_path(cursor->path, &pathlst);
			}
		}
		cursor = cursor->next;
	}
	if (pathlst != NULL)
		main_loop(pathlst, opflag);
}
