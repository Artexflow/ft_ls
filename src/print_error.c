/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 05:09:09 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/12 05:16:16 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <errno.h>
#include <libft.h>

int			print_error(char *incriminated_var, int ret)
{
	ft_putstr("ft_ls: ");
	if (incriminated_var)
	{
		ft_putstr(incriminated_var);
		ft_putstr(": ");
	}
	ft_putstr(strerror(errno));
	ft_putstr("\n");
	return (ret);
}
