/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_infos.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/15 06:26:44 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 07:37:09 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stddef.h>
#include <libft.h>
#include <ft_ls.h>

int				set_stat_info(t_direntlst *elem)
{
	struct stat	*stat_i;

	stat_i = NULL;
	if (!(stat_i = malloc(sizeof(struct stat))) || !elem->path)
		return (0);
	else if (lstat(elem->path, stat_i) < 0)
		return (print_error(elem->path, 0));
	elem->stat = stat_i;
	return (1);
}

void			set_infos(t_direntlst *lst, t_opflag *opflag)
{
	t_direntlst	*cursor;

	cursor = lst;
	(void)opflag;
	while (cursor != NULL)
	{
		if (!cursor->path
			&& !(cursor->path = path_builder(cursor->original_path,
					cursor->dirent->d_name)))
			continue ;
		set_stat_info(cursor);
		cursor = cursor->next;
	}
}
