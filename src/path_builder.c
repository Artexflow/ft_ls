/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_builder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nsierra- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/12 05:34:49 by nsierra-          #+#    #+#             */
/*   Updated: 2014/04/15 06:51:54 by nsierra-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

static size_t	ft_strlen(const char *str)
{
	const char	*keep;

	keep = str;
	while (*str != '\0')
		++str;
	return ((size_t)(str - keep));
}

char			*path_builder(char *path, char *file)
{
	char		*new_str;
	char		*keep;

	if (!(new_str = malloc(ft_strlen(path) + ft_strlen(file) + 2)))
		return (NULL);
	keep = new_str;
	while (*path != '\0')
	{
		*new_str = *path;
		++new_str;
		++path;
	}
	if (*(path - 1) != '/')
		*new_str++ = '/';
	while (*file != '\0')
	{
		*new_str = *file;
		++new_str;
		++file;
	}
	*new_str = '\0';
	return (keep);
}
